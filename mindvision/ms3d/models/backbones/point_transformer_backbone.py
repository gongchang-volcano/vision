# Copyright 2022 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""
PointTransformer Backbone Module
"""
from mindvision.ms3d.models.blocks import transformer, transition_down
import mindspore.nn as nn
import mindspore.ops as ops

__all__ = ["Backbone"]

@ClassFactory.register(ModuleType.BACKBONE)
class Backbone(nn.Cell):
    """
    PointTransformer Backbone
    """
    def __init__(self):
        super(Backbone, self).__init__()
        npoints = 1024
        nblocks = 4
        nneighbor = 16
        d_points = 6
        self.fc1 = nn.SequentialCell(
            nn.Dense(d_points, 32),
            nn.ReLU(),
            nn.Dense(32, 32)

        )
        self.transpose = ops.Transpose()
        self.transformer1 = transformer.Transformer(32, 512, nneighbor)
        self.transition_downs = nn.CellList()
        self.transformers = nn.CellList()
        for i in range(nblocks):
            channel = 32 * 2 ** (i + 1)
            self.transition_downs.append(
                transition_down.Transitiondown(npoints // 4 ** (i + 1), nneighbor,
                                               [channel // 2 + 3, channel, channel]))
            self.transformers.append(transformer.Transformer(channel, 512, nneighbor))
        self.nblocks = nblocks

    def construct(self, x):
        """
        PointTransformer Backbone Construct
        """
        x = self.transpose(x, (0, 2, 1))
        xyz = x[..., :3]
        y = self.fc1(x)
        points = self.transformer1(xyz, y)[0]
        xyz_and_feat = [(xyz, points)]
        for i in range(self.nblocks):
            xyz, points = self.transition_downs[i](xyz, points)
            points = self.transformers[i](xyz, points)[0]
            xyz_and_feat.append((xyz, points))
        return points, xyz_and_feat
